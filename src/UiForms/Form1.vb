﻿Imports System
Imports System.Collections.Generic
Imports System.Drawing
Imports System.IO
Imports System.Windows.Forms
Imports Cambio.Common
Imports Cambio.UiForms.My



Public Class Form1
    Private _reader As CIniReader

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        TB_SourcePath.Text = MySettings.Default.StartUpPath
    End Sub

    Private Sub TB_SourcePath_TextChanged(sender As Object, e As EventArgs) Handles TB_SourcePath.TextChanged
        SetContentState()
    End Sub

    Private Sub B_SelectFile_Click(sender As Object, e As EventArgs) Handles B_SelectFile.Click
        dim ofd = new OpenFileDialog
        ofd.CheckFileExists = True
        ofd.Multiselect = false
        ofd.ReadOnlyChecked = true
        ofd.ShowReadOnly = true
        ofd.AddExtension = true
        ofd.AutoUpgradeEnabled = true
        ofd.CheckPathExists = true
        ofd.DefaultExt = "ini"
        ofd.DereferenceLinks = true
        ofd.Filter = "INI files (*.ini)|*.ini|All files (*.*)|*.*"
        ofd.SupportMultiDottedExtensions = True
        ofd.ValidateNames = True

        Dim result = ofd.ShowDialog(me)
        if (result = DialogResult.OK)
            TB_SourcePath.Text = ofd.FileName
        End If
    End Sub

    Private Sub B_Parse_Click(sender As Object, e As EventArgs) Handles B_Parse.Click
        If (not Parse())
            ClearContent()
        End If
        SetContentState()
    End Sub

    Private Sub CB_Sections_SelectedIndexChanged(sender As Object, e As EventArgs) Handles CB_Sections.SelectedIndexChanged
        Dim source = CType(sender, ComboBox)
        If (Not ReferenceEquals(Nothing, source.SelectedItem))
            dim sectionname = CType(source.SelectedItem, String)

            dim sectioncontent as IList(Of String) = nothing
            if (_reader.TryGetSection(sectionname, true, true, sectioncontent))
                sectioncontent.RemoveAt(0)
                LB_SectionContent.DataSource = sectioncontent
            End If
        End If
    End Sub

    Private Function GetPath() As String
        Try
            Return TB_SourcePath.Text
        Catch
            return nothing
        End Try
    End Function

    Private Function TryGetValidPath(ByRef value As String) As Boolean
        Try
            Dim path = GetPath()
            if ((not ReferenceEquals(Nothing, path)) AndAlso File.Exists(path))
                value = path
                Return true
            else
                Return false
            End If
        Catch
            Return false
        End Try
    End Function

    Private Function HasValidPath() As Boolean
        Dim path as String = nothing
        return TryGetValidPath(path)
    End Function

    Private Function Parse() As Boolean
        Dim path As String = nothing
        if (TryGetValidPath(path))
            Return Parse(path)
        Else
            SetSourceStatus(StatusLevelEnum.slWarn, "Invalid Path")
            _reader.Deinitialize()
            _reader = nothing
            return False
        End If
    End Function

    Private function Parse(path As String) As Boolean
        if (Not ReferenceEquals(Nothing, _reader))
            _reader.Deinitialize()
        else
            _reader = New CIniReader()
        End If
        Try
            Using s = File.OpenRead(path)
                If (_reader.Initialize(New StreamReader(s)))
                    CB_Sections.DataSource = _reader.GetAllSectionNames()
                    SetSourceStatus(StatusLevelEnum.slInformation, "Success")
                Else

                End If
            End Using
            SetContentState()
            Return true
        Catch ex As Exception
            if (Not ReferenceEquals(Nothing, _reader))
                _reader.Deinitialize()
                _reader = nothing
            End If
            SetSourceStatus(StatusLevelEnum.slError, ex.Message)
            Return false
        End Try
    End function

    Private sub ClearContent()
        CB_Sections.DataSource = nothing
        CB_Sections.Items.Clear()
        LB_SectionContent.DataSource = nothing
        LB_SectionContent.Items.Clear()
    End sub

    Private sub SetContentState()
        If (HasValidPath())
            B_Parse.Enabled = true
        else
            B_Parse.Enabled = false
        End If

        If (Not ReferenceEquals(Nothing, _reader))
            GB_Content.Enabled = true
            CB_Sections.Enabled = true
            LB_SectionContent.Enabled = true
        Else
            GB_Content.Enabled = false
            CB_Sections.Enabled = false
            LB_SectionContent.Enabled = false
            ClearContent()
        End If
    End sub

    Private enum StatusLevelEnum
        slInformation
        slWarn
        slError
    End enum

    Private sub SetSourceStatus(l as StatusLevelEnum, s As String)
        dim statusbar = SourceStatusBar
        statusbar.Items.Clear()

        If (Not ReferenceEquals(Nothing, s))
            Dim level = New ToolStripStatusLabel()
            level.Text = "   "
            level.Width = 10
            level.Height = 10
            select Case l
                Case StatusLevelEnum.slInformation
                    level.BackColor = Color.Green
                Case StatusLevelEnum.slWarn
                    level.BackColor = Color.Yellow
                Case StatusLevelEnum.slError
                    level.BackColor = Color.Red
            End Select
            statusbar.Items.Add(level)

            dim label = new ToolStripStatusLabel(s)
            statusbar.Items.Add(label)
        End If
    End sub    
End Class
