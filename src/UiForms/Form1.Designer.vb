﻿Imports System.Windows.Forms

<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.SourceStatusBar = New System.Windows.Forms.StatusStrip()
        Me.B_Parse = New System.Windows.Forms.Button()
        Me.B_SelectFile = New System.Windows.Forms.Button()
        Me.TB_SourcePath = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.GB_Content = New System.Windows.Forms.GroupBox()
        Me.LB_SectionContent = New System.Windows.Forms.ListBox()
        Me.CB_Sections = New System.Windows.Forms.ComboBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.GroupBox1.SuspendLayout
        Me.GB_Content.SuspendLayout
        Me.SuspendLayout
        '
        'GroupBox1
        '
        Me.GroupBox1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
        Me.GroupBox1.Controls.Add(Me.SourceStatusBar)
        Me.GroupBox1.Controls.Add(Me.B_Parse)
        Me.GroupBox1.Controls.Add(Me.B_SelectFile)
        Me.GroupBox1.Controls.Add(Me.TB_SourcePath)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 12)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(637, 116)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = false
        Me.GroupBox1.Text = "Source"
        '
        'SourceStatusBar
        '
        Me.SourceStatusBar.Location = New System.Drawing.Point(3, 91)
        Me.SourceStatusBar.Name = "SourceStatusBar"
        Me.SourceStatusBar.Size = New System.Drawing.Size(631, 22)
        Me.SourceStatusBar.SizingGrip = false
        Me.SourceStatusBar.TabIndex = 5
        '
        'B_Parse
        '
        Me.B_Parse.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
        Me.B_Parse.Location = New System.Drawing.Point(535, 53)
        Me.B_Parse.Name = "B_Parse"
        Me.B_Parse.Size = New System.Drawing.Size(96, 23)
        Me.B_Parse.TabIndex = 3
        Me.B_Parse.Text = "Parse"
        Me.B_Parse.UseVisualStyleBackColor = true
        '
        'B_SelectFile
        '
        Me.B_SelectFile.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
        Me.B_SelectFile.Location = New System.Drawing.Point(535, 17)
        Me.B_SelectFile.Name = "B_SelectFile"
        Me.B_SelectFile.Size = New System.Drawing.Size(96, 20)
        Me.B_SelectFile.TabIndex = 2
        Me.B_SelectFile.Text = "Select Path"
        Me.B_SelectFile.UseVisualStyleBackColor = true
        '
        'TB_SourcePath
        '
        Me.TB_SourcePath.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
        Me.TB_SourcePath.Location = New System.Drawing.Point(65, 17)
        Me.TB_SourcePath.Name = "TB_SourcePath"
        Me.TB_SourcePath.Size = New System.Drawing.Size(464, 20)
        Me.TB_SourcePath.TabIndex = 1
        '
        'Label1
        '
        Me.Label1.AutoSize = true
        Me.Label1.Location = New System.Drawing.Point(7, 20)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(52, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "INI Path :"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'GB_Content
        '
        Me.GB_Content.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom)  _
            Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
        Me.GB_Content.Controls.Add(Me.LB_SectionContent)
        Me.GB_Content.Controls.Add(Me.CB_Sections)
        Me.GB_Content.Controls.Add(Me.Label2)
        Me.GB_Content.Location = New System.Drawing.Point(13, 134)
        Me.GB_Content.Name = "GB_Content"
        Me.GB_Content.Size = New System.Drawing.Size(636, 360)
        Me.GB_Content.TabIndex = 1
        Me.GB_Content.TabStop = false
        Me.GB_Content.Text = "Source Content"
        '
        'LB_SectionContent
        '
        Me.LB_SectionContent.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom)  _
            Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
        Me.LB_SectionContent.FormattingEnabled = true
        Me.LB_SectionContent.Location = New System.Drawing.Point(9, 59)
        Me.LB_SectionContent.Name = "LB_SectionContent"
        Me.LB_SectionContent.Size = New System.Drawing.Size(621, 290)
        Me.LB_SectionContent.TabIndex = 2
        '
        'CB_Sections
        '
        Me.CB_Sections.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
        Me.CB_Sections.FormattingEnabled = true
        Me.CB_Sections.Location = New System.Drawing.Point(64, 19)
        Me.CB_Sections.Name = "CB_Sections"
        Me.CB_Sections.Size = New System.Drawing.Size(566, 21)
        Me.CB_Sections.TabIndex = 1
        '
        'Label2
        '
        Me.Label2.AutoSize = true
        Me.Label2.Location = New System.Drawing.Point(6, 22)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(54, 13)
        Me.Label2.TabIndex = 0
        Me.Label2.Text = "Sections :"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6!, 13!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(661, 503)
        Me.Controls.Add(Me.GB_Content)
        Me.Controls.Add(Me.GroupBox1)
        Me.Name = "Form1"
        Me.Text = "Form1"
        Me.GroupBox1.ResumeLayout(false)
        Me.GroupBox1.PerformLayout
        Me.GB_Content.ResumeLayout(false)
        Me.GB_Content.PerformLayout
        Me.ResumeLayout(false)

End Sub

    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents B_SelectFile As Button
    Friend WithEvents TB_SourcePath As TextBox
    Friend WithEvents Label1 As Label
    Friend WithEvents B_Parse As Button
    Friend WithEvents GB_Content As GroupBox
    Friend WithEvents CB_Sections As ComboBox
    Friend WithEvents Label2 As Label
    Friend WithEvents LB_SectionContent As ListBox
    Friend WithEvents SourceStatusBar As StatusStrip
End Class
