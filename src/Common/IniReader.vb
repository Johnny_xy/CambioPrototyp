﻿Imports System
Imports System.Collections.Generic
Imports System.ComponentModel
Imports System.IO
Imports System.Text
Imports Microsoft.VisualBasic



Public Class CIniReader

#Region "private fields"

    Private _content As IDictionary(Of String, IEnumerable(Of IIniLine))

#End Region

#Region "private types"

    Private enum LineTypeEnum
        Unknown
        Comment
        Section
        KeyValue
    End enum

    Private Interface IIniLine
        Property LineType as LineTypeEnum
    End Interface

    Private class SectionNameData
        Implements IIniLine
        Private ReadOnly _source As String
        Private ReadOnly _startIndex as Int32
        Private ReadOnly _endIndex as Int32

        Property LineType As LineTypeEnum Implements IIniLine.LineType
            Get
                Return LineTypeEnum.Section
            End Get
            Set
            End Set
        End Property

        Public ReadOnly Property RawName As String
            Get
                Return _source.Substring(_startIndex, _endIndex - _startIndex)
            End Get
        End Property

        Public ReadOnly Property Name As String
            Get
                Return Unescape(_source, _startIndex, _endIndex)
            End Get
        End Property

        Public Overrides Function ToString() As String
            Return Name
        End Function

        Public Sub New(source As String, startIndex As Int32, endIndex As Int32)
            _source = source
            _startIndex = startIndex
            _endIndex = endIndex
        End Sub
    End Class

    Private class KeyValueData
        Implements IIniLine
        Private ReadOnly _source As String
        Private ReadOnly _keyStartIndex as Int32
        Private ReadOnly _keyEndIndex as Int32
        Private ReadOnly _valueStartIndex as Int32
        Private ReadOnly _valueEndIndex as Int32

        Property LineType As LineTypeEnum Implements IIniLine.LineType
            Get
                Return LineTypeEnum.KeyValue
            End Get
            Set
            End Set
        End Property

        Public ReadOnly Property RawKey As String
            Get
                Return _source.Substring(_keyStartIndex, _keyEndIndex - _keyStartIndex)
            End Get
        End Property

        Public ReadOnly Property RawValue As String
            Get
                Return _source.Substring(_valueStartIndex, _valueEndIndex - _valueStartIndex)
            End Get
        End Property

        Public ReadOnly Property Key As String
            Get
                Return Unescape(_source, _keyStartIndex, _keyEndIndex)
            End Get
        End Property

        Public ReadOnly Property Value As String
            Get
                Return Unescape(_source, _valueStartIndex, _valueEndIndex)
            End Get
        End Property

        Public Overrides Function ToString() As String
            Return Key & " = " & Value
        End Function

        Public Sub New(source As String, keyStartIndex As Int32, keyEndIndex As Int32, valueStartIndex As Int32, valueEndIndex As Int32)
            _source = source
            _keyStartIndex = keyStartIndex
            _keyEndIndex = keyEndIndex
            _valueStartIndex = valueStartIndex
            _valueEndIndex = valueEndIndex
        End Sub
    End Class

    Private class CommentData
        Implements IIniLine
        Private ReadOnly _source As String
        Private ReadOnly _startIndex as Int32
        Private ReadOnly _endIndex as Int32

        Property LineType As LineTypeEnum Implements IIniLine.LineType
            Get
                Return LineTypeEnum.Comment
            End Get
            Set
            End Set
        End Property

        Public ReadOnly Property RawComment As String
            Get
                Return _source.Substring(_startIndex, _endIndex - _startIndex)
            End Get
        End Property

        Public ReadOnly Property Comment As String
            Get
                Return Unescape(_source, _startIndex, _endIndex)
            End Get
        End Property

        Public Overrides Function ToString() As String
            Return Comment
        End Function

        Public Sub New(source As String, startIndex As Int32, endIndex As Int32)
            _source = source
            _startIndex = startIndex
            _endIndex = endIndex
        End Sub
    End Class

    Private class UnknownData
        Implements IIniLine
        Private ReadOnly _source As String
        Private ReadOnly _startIndex as Int32
        Private ReadOnly _endIndex as Int32

        Property LineType As LineTypeEnum Implements IIniLine.LineType
            Get
                Return LineTypeEnum.Unknown
            End Get
            Set
            End Set
        End Property

        Public ReadOnly Property RawValue As String
            Get
                Return _source.Substring(_startIndex, _endIndex - _startIndex)
            End Get
        End Property

        Public ReadOnly Property Value As String
            Get
                Return Unescape(_source, _startIndex, _endIndex)
            End Get
        End Property

        Public Overrides Function ToString() As String
            Return RawValue
        End Function

        Public Sub New(source As String, startIndex As Int32, endIndex As Int32)
            _source = source
            _startIndex = startIndex
            _endIndex = endIndex
        End Sub
    End Class

#End Region

#Region "public methods"

    public Function GetAllSectionNames() As IList(Of String)
        Dim returnvalue = New List(Of String)
        For Each key As String In _content.Keys
            returnvalue.Add(key)
        Next
        Return returnvalue
    End Function

    public Function HasSection(sectionName As String) As Boolean
        Return _content.ContainsKey(sectionName)
    End Function

    public Function TryGetSection(sectionName As String, ignoreCommentLines as Boolean, ignoreUnknownLines as Boolean, Byref section As IList(Of String)) As Boolean
        Dim sectioncontent As IEnumerable(Of IIniLine) = Nothing

        if (_content.TryGetValue(sectionName, sectioncontent))
            Dim returnvalue = new List(Of String)
            For Each line As IIniLine In sectioncontent
                Select Case line.LineType
                    Case LineTypeEnum.Comment
                        If (Not ignoreCommentLines)
                            returnvalue.Add(line.ToString())
                        End If
                    Case LineTypeEnum.Unknown
                        If (Not ignoreUnknownLines)
                            returnvalue.Add(line.ToString())
                        End If
                    Case LineTypeEnum.KeyValue
                        returnvalue.Add(line.ToString())
                    Case LineTypeEnum.Section
                        returnvalue.Add(line.ToString())
                End Select
            Next
            section = returnvalue
            Return true
        Else
            Return false
        End If
    End Function

    public Function GetSection(sectionName As String, ignoreCommentLines as Boolean, ignoreUnknownLines as Boolean) As IList(Of String)
        Dim sectioncontent As IList(Of String) = Nothing

        if (TryGetSection(sectionName, ignoreCommentLines, ignoreUnknownLines, sectioncontent))
            Return sectioncontent
        Else
            Return Nothing
        End If
    End Function

    public Function TryGetValue(sectionName As String, key As String, byref value As String) As Boolean
        Dim section As IEnumerable(Of IIniLine) = Nothing

        if (_content.TryGetValue(sectionName, section))
            For Each line As IIniLine In section
                if (line.LineType = LineTypeEnum.KeyValue)
                    Dim linedata = CType(line, KeyValueData)
                    If ((linedata.Key.Length = key.Length) AndAlso (String.Compare(linedata.Key, 0, key, 0, key.Length) = 0))
                        value = linedata.Value
                        Return true
                    End If
                End If
            Next
        End If
        Return false
    End Function

    public Function GetValue(sectionName as String, key As String) As String
        Dim value As String = nothing

        If (TryGetValue(sectionName, key, value))
            Return value
        else
            Return nothing
        End If
    End Function

    public function Initialize(source As TextReader) As Boolean
        Dim data = ParseSource(source)
        If (Not ReferenceEquals(nothing, data))
            _content = CreateContent(data)
            Return true
        Else
            Return false
        end if
    End function

    public function Initialize(source As String) As Boolean
        Return Initialize(New StringReader(source))
    End function

    public sub Deinitialize()
        _content = Nothing
    End sub

#End Region

#Region "private methods"

    Private Shared sub VerifyParameters(source As String, startIndex As Int32, endIndex As Int32)
        If (ReferenceEquals(Nothing, source))
            Throw New ArgumentNullException("source")
        End If
        if (startIndex < 0)
            Throw new ArgumentOutOfRangeException("startIndex", startIndex, "value cannot be less than 0 (zero)")
        End If
        if (endIndex < 0)
            Throw new ArgumentOutOfRangeException("endIndex", endIndex, "value cannot be less than 0 (zero)")
        End If
        if (endIndex > source.Length)
            Throw new ArgumentOutOfRangeException("endIndex", endIndex, "endIndex cannot be greater than source length ({source length: " & source.Length & "}, {endIndex: " & endIndex & "})")
        End If
    End sub

    Private Shared Function IsEscapedCharacter(source As String, startIndex As Int32, endIndex As Int32, index As Int32) As Boolean
        VerifyParameters(source, startIndex, endIndex)

        if (index < startIndex)
            Throw new ArgumentOutOfRangeException("index", index, "value cannot be less than 0 (zero)")
        End If
        if ((index < startIndex) OrElse (index > endIndex))
            Throw new ArgumentOutOfRangeException("index", index, "index cannot be less than startIndex or greater than endIndex ({startIndex: " & startIndex & "}, {endIndex: " & endIndex & "}, {index: " & index & "})")
        End If

        Return ((index <> startIndex) AndAlso (index <> endIndex) AndAlso (source.Chars(index - 1) = "\"C))
    End Function

    Private Shared Function TrimStringLeadingSpaces(source As String, startIndex As Int32, endIndex As Int32) As Int32
        VerifyParameters(source, startIndex, endIndex)

        dim index = startIndex
        While (index <> endIndex)
            Dim c = source.Chars(index)
            if (c <> " "C)
                Return index
            End If
            index += 1
        end While

        Return endIndex
    End Function

    Private Shared Function ParseComment(source As String, startIndex As Int32, endIndex As Int32) As IIniLine
        VerifyParameters(source, startIndex, endIndex)

        Dim s = startIndex
        Dim e = endIndex
        dim i = TrimStringLeadingSpaces(source, startIndex, endIndex)
        if (i <> e)
            dim c = source.Chars(i)
            if ((c = ";"C) OrElse (c = "#"C) AndAlso (Not IsEscapedCharacter(source, s, e, i)))
                Return New CommentData(source, i + 1, e)
            End If
        End If

        return New UnknownData(source, startIndex, endIndex)
    End Function

    Private Shared Function ParseSectionHead(source As String, startIndex As Int32, endIndex As Int32) As IIniLine
        VerifyParameters(source, startIndex, endIndex)

        dim s = startIndex
        dim e = endIndex
        dim i = TrimStringLeadingSpaces(source, s, e)
        if (i <> e)
            dim c = source.Chars(i)
            if (c <> "["C)
                return New UnknownData(source, startIndex, endIndex)
            else
                i += 1
            End If
        End If

        s = i
        i = TrimStringLeadingSpaces(source, s, e)
        while (i <> e)      
            dim c = source.Chars(i)
            if ((c = "]"C) AndAlso (not IsEscapedCharacter(source, s, e, i)))
                if (i <> s)
                    return New SectionNameData(source, s, i)
                Else
                    Exit While
                End If
            End If
            i += 1
        End While

        return New UnknownData(source, startIndex, endIndex)
    End Function

    Private Shared Function ParseKeyValue(source As String, startIndex As Int32, endIndex As Int32) As IIniLine
        VerifyParameters(source, startIndex, endIndex)

        dim s = startIndex
        dim e = endIndex
        dim i = startIndex

        while (i <> e)      
            dim c = source.Chars(i)
            if ((c = "="C) AndAlso (not IsEscapedCharacter(source, s, e, i)))
                if (i <> s)
                    Return new KeyValueData(source, s, i, i + 1, e)
                Else
                    Exit While
                End If
            End If
            i += 1
        End While

        return New UnknownData(source, startIndex, endIndex)
    End Function

    Private Shared Function Unescape(source As String, startIndex As Int32, endIndex As Int32) As String
        VerifyParameters(source, startIndex, endIndex)

        dim returnvalue = new StringBuilder(source.Length + 512)

        dim s = startIndex
        dim e = endIndex
        while (s <> e)            
            dim i = source.IndexOf("\", s, e - s, StringComparison.OrdinalIgnoreCase)
            if (i = - 1)
                i = e
            End If

            returnvalue.Append(source, s, i - s)
            If (i <> e)
                if ((i + 1) <> e)
                    Dim c = source.Chars(i + 1)
                    Select Case c
                        Case "\"C
                            returnvalue.Append("\"C)
                        Case "0"C
                            returnvalue.Append(Chr(0))
                        Case "a"C
                            returnvalue.Append(Chr(7))
                        Case "b"C
                            returnvalue.Append(vbBack)
                        Case "t"C
                            returnvalue.Append(vbTab)
                        Case "r"C
                            returnvalue.Append(vbCr)
                        Case "n"C
                            returnvalue.Append(vbLf)
                        Case ";"C
                            returnvalue.Append(";"C)
                        Case "#"C
                            returnvalue.Append("#"C)
                        Case "="C
                            returnvalue.Append("="C)
                        Case ":"C
                            returnvalue.Append(":"C)
                        case Else
                            returnvalue.Append("\"C)
                            returnvalue.Append(c)
                    End Select

                    i += 1
                Else
                    returnvalue.Append(source.Chars(i))
                End If

                i += 1
            end if

            s = i
        End While

        Return returnvalue.ToString()
    End Function

    Private Shared Function ParseSource(source As String, startIndex As Int32, endIndex As Int32) As IIniLine
        VerifyParameters(source, startIndex, endIndex)

        Dim line = ParseComment(source, startIndex, endIndex)
        if (line.LineType = LineTypeEnum.Comment)
            Return line
        End If

        line = ParseSectionHead(source, startIndex, endIndex)
        if (line.LineType = LineTypeEnum.Section)
            Return line
        End If

        line = ParseKeyValue(source, startIndex, endIndex)
        if (line.LineType = LineTypeEnum.KeyValue)
            Return line
        End If

        Return line
    End Function

    private Shared Function ParseSource(source As TextReader) As IList(Of IIniLine)
        If (ReferenceEquals(Nothing, source))
            Throw New ArgumentNullException("source")
        End If

        Try
            Dim returnvalue = new List(Of IIniLine)

            While (true)
                dim line = source.ReadLine()
                if (Not ReferenceEquals(Nothing, line))
                    returnvalue.Add(ParseSource(line, 0, line.Length))
                Else
                    exit while
                End If
            End While

            Return returnvalue
        Catch
            Return nothing
        End Try
    End Function

    Private Shared Function CreateContent(source As IEnumerable(Of IIniLine)) As IDictionary(Of String, IEnumerable(Of IIniLine))

        Dim content As IDictionary(Of String, IEnumerable(Of IIniLine)) = New Dictionary(Of String, IEnumerable(Of IIniLine))
        Dim currentsection As List(Of IIniLine) = nothing

        For Each line As IIniLine In source
            Select case (line.LineType)
                Case LineTypeEnum.Section :
                    Dim data = CType(line, SectionNameData)
                    If (Not content.ContainsKey(data.Name))
                        currentsection = New List(Of IIniLine)()
                        content.Add(data.Name, currentsection)
                        currentsection.Add(line)
                    End If
                Case LineTypeEnum.KeyValue :
                    If (Not ReferenceEquals(nothing, currentsection))
                        currentsection.Add(line)
                    End If
                Case LineTypeEnum.Comment :
                    If (Not ReferenceEquals(nothing, currentsection))
                        currentsection.Add(line)
                    End If
                Case LineTypeEnum.Unknown :
                    If (Not ReferenceEquals(nothing, currentsection))
                        currentsection.Add(line)
                    End If
                Case else
                    Throw New InvalidEnumArgumentException()
            End Select
        Next

        Return content
    End Function

#End Region
End Class
